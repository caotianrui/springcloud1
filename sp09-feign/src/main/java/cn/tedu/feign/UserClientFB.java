package cn.tedu.feign;

import cn.tedu.sp01.pojo.User;
import cn.tedu.web.util.JsonResult;
import org.springframework.stereotype.Component;

@Component
public class UserClientFB implements UserClient {
    @Override
    public JsonResult<User> getUser(Integer userId) {
        return JsonResult.err().msg("获取用户信息失败");
    }

    @Override
    public JsonResult<User> addScore(Integer userId, Integer score) {
        return JsonResult.err().msg("增加积分失败");
    }
}
