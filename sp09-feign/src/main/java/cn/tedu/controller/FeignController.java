package cn.tedu.controller;

import cn.tedu.feign.ItemClient;
import cn.tedu.feign.OrderClient;
import cn.tedu.feign.UserClient;
import cn.tedu.sp01.pojo.Item;
import cn.tedu.sp01.pojo.Order;
import cn.tedu.sp01.pojo.User;
import cn.tedu.web.util.JsonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Slf4j
public class FeignController {
    @Autowired
    private ItemClient itemClient;
    @Autowired
    private UserClient userClient;
    @Autowired
    private OrderClient orderClient;


    @GetMapping("/item-service/{orderId}")
    public JsonResult<List<Item>> getItems(@PathVariable String orderId){
        //rt.getForObject(http://item-service/{1})
        //通过声明式客户端接口，调用远程商品服务获取商品列表功能
        return itemClient.getItems(orderId);
    }

    @PostMapping("item-service/decreaseNumber")
    public JsonResult<?> decreaseNumber(@RequestBody List<Item> items){
        return itemClient.decreaseNumber(items);
    }

    @GetMapping("/user-service/{userId}")
    public JsonResult<User> getUser(@PathVariable Integer userId){
        return userClient.getUser(userId);
    }

    @GetMapping("/user-service/{userId}/score")
    public JsonResult<User> addScore(@PathVariable Integer userId,@RequestParam Integer score){
        return userClient.addScore(userId,score);
    }

    @GetMapping("/order-service/{orderId}")
    public JsonResult<Order> getOrder(@PathVariable String orderId){
        return orderClient.getOrder(orderId);
    }
    @GetMapping("/order-service")
    public JsonResult<?> addOrder(){
        return orderClient.addOrder();
    }
}
