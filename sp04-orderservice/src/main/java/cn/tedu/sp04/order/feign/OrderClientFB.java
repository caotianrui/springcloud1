package cn.tedu.sp04.order.feign;

import cn.tedu.sp01.pojo.Order;
import cn.tedu.web.util.JsonResult;
import org.springframework.stereotype.Component;

@Component
public class OrderClientFB implements OrderClient {
    @Override
    public JsonResult<Order> getOrder(String orderId) {
        return JsonResult.err().msg("获取订单失败");
    }

    @Override
    public JsonResult<?> addOrder() {
        return JsonResult.err().msg("增加订单失败");
    }
}
