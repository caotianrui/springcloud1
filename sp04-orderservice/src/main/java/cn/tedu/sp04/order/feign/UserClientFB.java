package cn.tedu.sp04.order.feign;

import cn.tedu.sp01.pojo.Item;
import cn.tedu.sp01.pojo.User;
import cn.tedu.web.util.JsonResult;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserClientFB implements UserClient {
    @Override
    public JsonResult<User> getUser(Integer userId) {

          if(Math.random() < 0.5){
            User user = new User(1, "缓存用户8", "缓存密码8");
            return JsonResult.ok(user);
        }return JsonResult.err().msg("获取用户信息失败");
    }

    @Override
    public JsonResult<User> addScore(Integer userId, Integer score) {
        return JsonResult.err().msg("增加积分失败");
    }
}
