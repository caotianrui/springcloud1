package cn.tedu.sp04.order.feign;

import cn.tedu.sp01.pojo.Item;
import cn.tedu.web.util.JsonResult;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ItemClientFB implements ItemClient{
    @Override
    public JsonResult<List<Item>> getItems(String orderId) {
        //可以返回缓存数据
        //这里模拟返回缓存数据
        if(Math.random() < 0.5){
            List<Item> list = new ArrayList<>();
            list.add(new Item(1,"缓存商品1",1));
            list.add(new Item(2,"缓存商品2",2));
            list.add(new Item(3,"缓存商品3",3));
            list.add(new Item(4,"缓存商品4",4));
            list.add(new Item(5,"缓存商品5",5));
            return JsonResult.ok(list);
        }

        return JsonResult.err().msg("获取订单的商品列表失败");
    }

    @Override
    public JsonResult<?> decreaseNumber(List<Item> items) {
        return JsonResult.err().msg("减少商品库存失败");
    }
}
