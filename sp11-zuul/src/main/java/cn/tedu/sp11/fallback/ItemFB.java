package cn.tedu.sp11.fallback;

import cn.tedu.web.util.JsonResult;
import com.netflix.zuul.context.RequestContext;
import org.springframework.cloud.netflix.zuul.filters.route.FallbackProvider;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

@Component
public class ItemFB implements FallbackProvider {
    //返回service id 针对指定的服务进行降级处理
    //返回 "*" 或者null 对所有服务都应用降级
    @Override
    public String getRoute() {
        return "item-service";
    }

    //降级响应 返回一个封装响应数据的response 对象
    @Override
    public ClientHttpResponse fallbackResponse(String route, Throwable cause) {


        return new ClientHttpResponse() {
            @Override
            public HttpStatus getStatusCode() throws IOException {
                return HttpStatus.OK;
            }

            @Override
            public int getRawStatusCode() throws IOException {
                return HttpStatus.OK.value();
            }

            @Override
            public String getStatusText() throws IOException {
                return HttpStatus.OK.getReasonPhrase();
            }

            @Override
            public void close() {

            }

            @Override
            public InputStream getBody() throws IOException {
                String json =
                        JsonResult.err().msg("调用商品服务失败").toString();
                return new ByteArrayInputStream(json.getBytes(StandardCharsets.UTF_8));
            }

            @Override
            public HttpHeaders getHeaders() {
                //Content-Type  :alpplication/json;charset=utf-8
                HttpHeaders httpHeaders = new HttpHeaders();
                httpHeaders.add("Content-Type","application/json;charset=UTF-8");
                return httpHeaders;
            }
        };
    }
}
