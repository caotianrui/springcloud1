package cn.tedu.sp11.filter;

import cn.tedu.web.util.JsonResult;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;


import javax.servlet.http.HttpServletRequest;

@Component
public class AccessFilter extends ZuulFilter {
    //过滤器类型：pre post routing error
    @Override
    public String filterType() {
        return FilterConstants.PRE_TYPE;
    }

    //过滤器插入的位置
    @Override
    public int filterOrder() {
        return 6;
    }

    //对用户请求进行判断 是否要执行过滤代码
    @Override
    public boolean shouldFilter() {
        /**
         * 只对item-service 调用进行过滤
         * 如果调用的后台是user order 不执行过滤代码
         */
        //获取调用的服务id
        RequestContext ctx = RequestContext.getCurrentContext();
        String serviceId = (String)ctx.get(FilterConstants.SERVICE_ID_KEY);
        if (StringUtils.hasLength(serviceId) && serviceId.equalsIgnoreCase("item-service")){
            return true;
        }
        return false;
    }

    //过滤代码
    @Override
    public Object run() throws ZuulException {
        //获取request对象
        RequestContext currentContext = RequestContext.getCurrentContext();
        HttpServletRequest request = currentContext.getRequest();
        //接收token 参数
        String token = request.getParameter("token");
        //如果没有token 停止访问 并直接向客户端返回响应
        if(!StringUtils.hasLength(token)){
            //阻止继续访问
            currentContext.setSendZuulResponse(false);
            //JsonResult {code: 400,mag: not login,data: null}
            String json = JsonResult.err().code(JsonResult.NOT_LOGIN).msg("not login  没有登录").data(null).toString();
            currentContext.addZuulRequestHeader("Content-Type","application/json");
            currentContext.setResponseBody(json);
            currentContext.getResponse().setCharacterEncoding("GBK");
        }
        return null; //返回值没有任何作用
    }
}
