package cn.tedu.sp06;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
@EnableCircuitBreaker
@SpringBootApplication
public class Sp06RibbonApplication {

	public static void main(String[] args) {
		SpringApplication.run(Sp06RibbonApplication.class, args);
	}

	//创建 RestTemplate 实例，放入 spring 容器
	@LoadBalanced //对RestTemplte进行封装，增强RestTemplate的功能
	@Bean
	public RestTemplate restTemplate(){
		SimpleClientHttpRequestFactory f = new SimpleClientHttpRequestFactory();
		//两个超时时间参数默认是-1，表示不启用
		f.setConnectTimeout(1000);
		f.setReadTimeout(1000);
		return new RestTemplate(f);
	}
}
